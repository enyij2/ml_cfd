# AlexNet.py, found on github, modified by Olivia Jiang
# number of training examples = 3476
# number of test examples = 940
# X_train shape: (3476, 201, 201, 4)
# Y_train shape: (940, 2)
# X_test shape: (3476, 201, 201, 4)
# Y_test shape: (940, 2)

import tensorflow as tf
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
#from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
#import matplotlib.pyplot as plt
from scipy import stats

class AlexNet(object):
    """Implementation of the AlexNet."""

    def __init__(self, x, keep_prob, num_classes, skip_layer,
                 weights_path='DEFAULT'):
        """Create the graph of the AlexNet model.
        Args:
            x: Placeholder for the input tensor.
            keep_prob: Dropout probability.
            num_classes: Number of classes in the dataset.
            skip_layer: List of names of the layer, that get trained from
                scratch
            weights_path: Complete path to the pretrained weight file, if it
                isn't in the same folder as this code
        """
        # Parse input arguments into class variables
        self.X = x
        self.NUM_CLASSES = num_classes
        self.KEEP_PROB = keep_prob
        self.SKIP_LAYER = skip_layer

        if weights_path == 'DEFAULT':
            self.WEIGHTS_PATH = 'bvlc_alexnet.npy'
        else:
            self.WEIGHTS_PATH = weights_path

        # Call the create function to build the computational graph of AlexNet
        self.create()

    def create(self):
        """Create the network graph."""
        # 1st Layer: Conv (w ReLu) -> Lrn -> Pool
        conv1 = conv(self.X, 11, 11, 96, 4, 4, padding='VALID', name='conv1')
        norm1 = lrn(conv1, 2, 2e-05, 0.75, name='norm1')
        pool1 = max_pool(norm1, 3, 3, 2, 2, padding='VALID', name='pool1')
        
        # 2nd Layer: Conv (w ReLu)  -> Lrn -> Pool with 2 groups
        conv2 = conv(pool1, 5, 5, 256, 1, 1, groups=2, name='conv2')
        norm2 = lrn(conv2, 2, 2e-05, 0.75, name='norm2')
        pool2 = max_pool(norm2, 3, 3, 2, 2, padding='VALID', name='pool2')
        
        # 3rd Layer: Conv (w ReLu)
        conv3 = conv(pool2, 3, 3, 384, 1, 1, name='conv3')

        # 4th Layer: Conv (w ReLu) splitted into two groups
        conv4 = conv(conv3, 3, 3, 384, 1, 1, groups=2, name='conv4')

        # 5th Layer: Conv (w ReLu) -> Pool splitted into two groups
        conv5 = conv(conv4, 3, 3, 256, 1, 1, groups=2, name='conv5')
        pool5 = max_pool(conv5, 3, 3, 2, 2, padding='VALID', name='pool5')

        # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
        pool5_shape = pool5.get_shape().as_list()
        flattened_shape = pool5_shape[1] * pool5_shape[2] * pool5_shape[3]
        flattened = tf.reshape(pool5, [-1, flattened_shape])
        fc6 = fc(flattened, flattened_shape, 4096, name='fc6')
        dropout6 = dropout(fc6, self.KEEP_PROB)

        # 7th Layer: FC (w ReLu) -> Dropout
        fc7 = fc(dropout6, 4096, 4096, name='fc7')
        dropout7 = dropout(fc7, self.KEEP_PROB)

        # 8th Layer: FC and return unscaled activations
        fc8 = fc(dropout7, 4096, 1000, relu=True, name='fc8')
        fc9 = fc(fc8, 1000, 200, relu=True, name='fc9')
        fc10 = fc(fc9, 200, 10, relu=True, name='fc10')
        self.fc11 = fc(fc10, 10, self.NUM_CLASSES, relu=False, name='fc11')

    def load_initial_weights(self, session):
        """Load weights from file into network.
        As the weights from http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/
        come as a dict of lists (e.g. weights['conv1'] is a list) and not as
        dict of dicts (e.g. weights['conv1'] is a dict with keys 'weights' &
        'biases') we need a special load function
        """
        # Load the weights into memory
        weights_dict = np.load(self.WEIGHTS_PATH, encoding='bytes').item()

        # Loop over all layer names stored in the weights dict
        for op_name in weights_dict:

            # Check if layer should be trained from scratch
            if op_name not in self.SKIP_LAYER:

                with tf.variable_scope(op_name, reuse=True):

                    # Assign weights/biases to their corresponding tf variable
                    for data in weights_dict[op_name]:

                        # Biases
                        if len(data.shape) == 1:
                            var = tf.get_variable('biases', trainable=False)
                            session.run(var.assign(data))

                        # Weights
                        else:
                            var = tf.get_variable('weights', trainable=False)
                            session.run(var.assign(data))


def conv(x, filter_height, filter_width, num_filters, stride_y, stride_x, name,
         padding='SAME', groups=1):
    """Create a convolution layer.
    Adapted from: https://github.com/ethereon/caffe-tensorflow
    """
    # Get number of input channels
    input_channels = int(x.get_shape()[-1])

    # Create lambda function for the convolution
    convolve = lambda i, k: tf.nn.conv2d(i, k,
                                         strides=[1, stride_y, stride_x, 1],
                                         padding=padding)

    with tf.variable_scope(name) as scope:
        # Create tf variables for the weights and biases of the conv layer
        weights = tf.get_variable('weights', shape=[filter_height,
                                                    filter_width,
                                                    input_channels/groups,
                                                    num_filters])
        biases = tf.get_variable('biases', shape=[num_filters])

    if groups == 1:
        conv = convolve(x, weights)

    # In the cases of multiple groups, split inputs & weights and
    else:
        # Split input and weights and convolve them separately
        input_groups = tf.split(axis=3, num_or_size_splits=groups, value=x)
        weight_groups = tf.split(axis=3, num_or_size_splits=groups,
                                 value=weights)
        output_groups = [convolve(i, k) for i, k in zip(input_groups, weight_groups)]

        # Concat the convolved output together again
        conv = tf.concat(axis=3, values=output_groups)

    # Add biases
    bias = tf.reshape(tf.nn.bias_add(conv, biases), tf.shape(conv))

    # Apply relu function
    relu = tf.nn.relu(bias, name=scope.name)

    return relu


def fc(x, num_in, num_out, name, relu=True):
    """Create a fully connected layer."""
    with tf.variable_scope(name) as scope:

        # Create tf variables for the weights and biases
        weights = tf.get_variable('weights', shape=[num_in, num_out],
                                  trainable=True)
        biases = tf.get_variable('biases', [num_out], trainable=True)

        # Matrix multiply weights and inputs and add bias
        act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)

    if relu:
        # Apply ReLu non linearity
        relu = tf.nn.relu(act)
        return relu
    else:
        return act


def max_pool(x, filter_height, filter_width, stride_y, stride_x, name,
             padding='SAME'):
    """Create a max pooling layer."""
    return tf.nn.max_pool(x, ksize=[1, filter_height, filter_width, 1],
                          strides=[1, stride_y, stride_x, 1],
                          padding=padding, name=name)


def lrn(x, radius, alpha, beta, name, bias=1.0):
    """Create a local response normalization layer."""
    return tf.nn.local_response_normalization(x, depth_radius=radius,
                                              alpha=alpha, beta=beta,
                                              bias=bias, name=name)


def dropout(x, keep_prob):
    """Create a dropout layer."""
    return tf.nn.dropout(x, keep_prob)
# cost function to calculate loss
def cost(logits, labels):
            with tf.name_scope('loss'):
                # cross_entropy = tf.losses.sparse_softmax_cross_entropy(labels=y_, logits=y_conv)
                mse = tf.losses.mean_squared_error(logits, labels)
                #tf.add_to_collection("losses",mse_loss)
                #loss = tf.add_n(tf.get_collection("losses"))
            return mse
#train function
def train(X_train, Y_train, X_test, Y_test):
    features = tf.placeholder(tf.float32, [None, 201, 201, 4])
    labels = tf.placeholder(tf.float32, [None, 2])
    keep_prob = 0.5

    model = AlexNet(features, keep_prob, 2, [])
    score = model.fc11
    # loss calculation
    mse = cost(score, labels)
    train_step = tf.train.AdamOptimizer(1e-3).minimize(mse)
    with tf.Session() as sess:
                    sess.run(tf.global_variables_initializer())
                    for i in range(1000):
                        shuffle = np.random.randint(0,1000,80)
                        X_mini_batch = X_train[shuffle] 
                        Y_mini_batch = Y_train[shuffle]
                        train_step.run(feed_dict={features: X_mini_batch, labels: Y_mini_batch})

                        if i % 50 == 0:
                            train_cost = sess.run(mse, feed_dict={features: X_mini_batch,
                                                  labels: Y_mini_batch})
                            print('step %d, training cost %g' % (i, train_cost))

                            test_cost = sess.run(mse, feed_dict={features: X_test,
                                                  labels: Y_test})
                            print('testing cost is %g' % (test_cost))
                    test_out = sess.run(score, feed_dict={features: X_test,
                                                  labels: Y_test})
                    np.savetxt('test_model_value.csv', test_out, delimiter=',')
                    train_out = sess.run(score, feed_dict={features: X_train[:100],
                                                  labels: Y_train[:100]})
                    np.savetxt('train_model_value.csv', train_out, delimiter=',')


targets = np.load( "target_input.npy" )
images = np.load("images.npy")
f = open('trans_target.csv')
f = f.readlines()
new_target = []
for line in f:
    line = line.strip('\n')
    line = line.split(',')
    new_target.append(line)

new_target = np.array(new_target)
#new_target = new_target[:,1].reshape(new_target.shape[0],1)
#print(new_target[:100])

# find outliers that are unphysics
outlier1 = np.where((targets[:,0] < 0) | (targets[:,0] > 1))
outlier1 = np.asarray(outlier1)
outlier1 = np.reshape(outlier1,(-1,) )
outlier_index = np.argsort(targets,axis=0)[-100:]
outlier_index = np.concatenate((outlier_index[:,0], outlier_index[:,1]),axis=0)
outlier_index = np.concatenate((outlier_index, outlier1),axis=0)
outlier_index = np.unique(outlier_index)

# delete these outliers 
targets = np.delete(targets,outlier_index,0)
images = np.delete(images,outlier_index,0)
#targets = (targets[:,0] * 100).reshape((targets.shape[0],1))
scaler_target = StandardScaler().fit(targets)
targets = scaler_target.transform(targets)
#targets = targets[:,1].reshape(targets.shape[0],1)
#target_train = target_train[:,0]
#scalers = {}
#for i in range(images.shape[0]):
#    for j in range(images.shape[3]):
#        scalers[i] = StandardScaler()
#        images[i, :, :, j] = scalers[i].fit_transform(images[i, :, :, j])
#    
#images_train, images_test, targets_train, targets_test = train_test_split(
#        images, targets, test_size=0.2, random_state=21)
scaler = {}
for i in range(images.shape[0]):
        for j in range(images.shape[3]):
            scaler[i] = StandardScaler()
            images[i,:,:,j] = scaler[i].fit_transform(images[i,:,:,j])
#print(scipy.stats.skew(images))
#print(scipy.stats.kurtosis(images))
#trans_target = stats.yeojohnson(targets)
#print(stats.skew(trans_target))
#print(stats.kurtosis(trans_target))
#targets = targets[:, 1.reshape((targets.shape[0],1))


images_train, images_test, targets_train, targets_test = train_test_split(
        images, new_target, test_size=0.2, random_state=21)
np.savetxt('test_real_value.csv', targets_test, delimiter=',', fmt='%s')
np.savetxt('train_real_value.csv', targets_train, delimiter=',', fmt='%s')
X_train, Y_train, X_test, Y_test = images_train, targets_train, images_test, targets_test
train(X_train, Y_train, X_test, Y_test)
