# ML_CFD

This is for the independent study for training the turbulence data for deep learning methods.
* ResNet
<br> Not suit for training regression for a limited amount of data. Easy to cause overfitting problem.
* VGG
<br> Performs better than ResNet when using some parts of the nets (not all the stages of convolutional process). Target 2 is much easier to train which Target 1 is not performing good enough.
* AlexNet
<br> At present the best choice. After doing the power transformation to the input target,
the distribution of the data becomes better and both Target 1 and 2 are closer to converge now.