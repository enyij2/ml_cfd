#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Author:ZhengzhengLiu, modified by Olivia Jiang
# number of training examples = 3476
# number of test examples = 940
# X_train shape: (3476, 201, 201, 4)
# Y_train shape: (940, 2)
# X_test shape: (3476, 201, 201, 4)
# Y_test shape: (940, 2)
 
import tensorflow as tf
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
#from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
#import matplotlib.pyplot as plt
import scipy
from sklearn.preprocessing import power_transform
#VGG_16 all use 3*3 kernels and 2*2 max_pooling
 
# create covolutional operation
def conv_op(input_op,name,kh,kw,n_out,dh,dw):
    """
    param :
    input_op -- input tensor
    kh -- height of the kernel
    kw -- width of the kernel
    n_out -- number of kernels
    dh -- stride height
    dw -- stride width
    p -- parameter
    return:
    A -- output of the convolutional layer
    """
    n_in = input_op.get_shape()[-1].value  
    #print(n_in)
 
    with tf.variable_scope(name) as scope:
        weights = tf.get_variable("weight", shape=[kh,kw,n_in,n_out],dtype=tf.float32,
                                  initializer=tf.contrib.layers.xavier_initializer())
        biases = tf.get_variable("bias", shape=[n_out],dtype=tf.float32,
                                 initializer=tf.constant_initializer(0.0),trainable=True)
        conv = tf.nn.conv2d(input=input_op,filter=weights,strides=[1,dh,dw,1],padding="SAME")
        Z = tf.nn.bias_add(conv,biases)
        #print(Z.shape)
        A = tf.nn.relu(Z)
 
        return A
 
# maxpooling layer
def maxpool_op(input_op,name,kh,kw,dh,dw):
    """
    param :
    input_op -- input tensor
    kh -- height of the kernel
    kw -- width of the kernel
    dh -- stride height
    dw -- stride width
    p -- parameter
    return:
    pool -- object to be max_pooled
    """
    pool = tf.nn.max_pool(input_op,ksize=[1,kh,kw,1],strides=[1,dh,dw,1],padding="SAME",name=name)
    return pool
 
# fully-connected layer
def fc_op(input_op,name,n_out):
    """
    param :
    input_op -- input tensor
    n_out -- number of kernels
    p -- parameter
    return:
    A -- fully connected layer output
    """
    n_in = input_op.get_shape()[-1].value
    with tf.variable_scope(name) as scope:
        weights = tf.get_variable("weight", shape=[n_in,n_out],dtype=tf.float32,
                                  initializer=tf.contrib.layers.xavier_initializer())

        biases = tf.get_variable("bias", shape=[n_out],dtype=tf.float32,
                                 initializer=tf.constant_initializer(0.1))

        A = tf.nn.relu_layer(input_op,weights,biases)
 

 
        return A
 
# the framework of VGG-16 network
def VGG_16(input_op):
    """
    param :
    input_op -- input tensor
    return:
    fc9 -- last fully-connected layer to be returned
    """
 
    # first
    #print(input_op.shape)
    conv1_1 = conv_op(input_op,name="conv1_1",kh=3,kw=3,n_out=16,dh=1,dw=1)
    conv1_2 = conv_op(conv1_1,name="conv1_2",kh=3,kw=3,n_out=16,dh=1,dw=1)
    #最大池化层采用的尺寸大小为：2*2，步长s=2，输出为：112*112*16
    pool1 = maxpool_op(conv1_2,name="pool1",kh=2,kw=2,dh=2,dw=2)
 
    # second
    conv2_1 = conv_op(pool1,name="conv2_1",kh=3,kw=3,n_out=32,dh=1,dw=1)
    conv2_2 = conv_op(conv2_1,name="conv2_2",kh=3,kw=3,n_out=32,dh=1,dw=1)

    pool2 = maxpool_op(conv2_2,name="pool2",kh=2,kw=2,dh=2,dw=2)
 
    ## third
    #conv3_1 = conv_op(pool2,name="conv3_1",kh=3,kw=3,n_out=64,dh=1,dw=1)
    #conv3_2 = conv_op(conv3_1,name="conv3_2",kh=3,kw=3,n_out=64,dh=1,dw=1)
    #conv3_3 = conv_op(conv3_2,name="conv3_3",kh=3,kw=3,n_out=64,dh=1,dw=1)
#
    #pool3 = maxpool_op(conv3_3,name="pool3",kh=2,kw=2,dh=2,dw=2)
 #
    ## fourth
    #conv4_1 = conv_op(pool3, name="conv4_1", kh=3, kw=3, n_out=128, dh=1, dw=1)
    #conv4_2 = conv_op(conv4_1, name="conv4_2", kh=3, kw=3, n_out=128, dh=1, dw=1)
    #conv4_3 = conv_op(conv4_2, name="conv4_3", kh=3, kw=3, n_out=128, dh=1, dw=1)
#
    #pool4 = maxpool_op(conv4_3, name="pool4", kh=2, kw=2, dh=2, dw=2)
 #
    ## fifth
    #conv5_1 = conv_op(pool4, name="conv5_1", kh=3, kw=3, n_out=256, dh=1, dw=1)
    #conv5_2 = conv_op(conv5_1, name="conv5_2", kh=3, kw=3, n_out=256, dh=1, dw=1)
    #conv5_3 = conv_op(conv5_2, name="conv5_3", kh=3, kw=3, n_out=256, dh=1, dw=1)
#
    #pool5 = maxpool_op(conv5_3, name="pool5", kh=2, kw=2, dh=2, dw=2)
 

    pool5_shape = pool2.get_shape().as_list()
    flattened_shape = pool5_shape[1] * pool5_shape[2] * pool5_shape[3]
    dense = tf.reshape(pool2, shape=[-1, flattened_shape],name="dense")  # 向量化
 
    fc6 = fc_op(dense,name="fc6",n_out=300)
    fc6 = tf.layers.batch_normalization(fc6)
    fc6 = tf.nn.relu(fc6)
    #fc6_drop = tf.nn.dropout(fc6,keep_prob=keep_prob,name="fc6_drop")
 
    fc7 = fc_op(fc6, name="fc7", n_out=20)
    fc7 = tf.layers.batch_normalization(fc7)
    fc7 = tf.nn.relu(fc7)
    #fc7_drop = tf.nn.dropout(fc7, keep_prob=keep_prob, name="fc7_drop")
 
    fc8 = fc_op(fc7,name="fc8",n_out=2)
    #softmax = tf.nn.softmax(fc8)
    #fc8 = tf.layers.batch_normalization(fc8)
    #fc8 = tf.nn.relu(fc8)
    ##fc8_drop = tf.nn.dropout(fc8, keep_prob=keep_prob, name="fc8_drop")
    ##prediction = tf.argmax(softmax,1)
    #fc9 = fc_op(fc8,name="fc9",n_out=2)
 
    return fc8 # return the last 

# cost function to calculate loss
def cost(logits, labels):
            with tf.name_scope('loss'):
                # cross_entropy = tf.losses.sparse_softmax_cross_entropy(labels=y_, logits=y_conv)
                mse = tf.losses.mean_squared_error(logits, labels)
            return mse
#train function
def train(X_train, Y_train, X_test, Y_test):
    features = tf.placeholder(tf.float32, [None, 201, 201, 4])
    labels = tf.placeholder(tf.float32, [None, 2])

    output = VGG_16(features)
    # loss calculation
    mse = cost(output, labels)
    train_step = tf.train.AdamOptimizer(1e-3).minimize(mse)
    with tf.Session() as sess:
                    sess.run(tf.global_variables_initializer())
                    for i in range(400):
                        shuffle = np.random.randint(0,1000,80)
                        X_mini_batch = X_train[shuffle] 
                        Y_mini_batch = Y_train[shuffle]
                        train_step.run(feed_dict={features: X_mini_batch, labels: Y_mini_batch})

                        if i % 50 == 0:
                            train_cost = sess.run(mse, feed_dict={features: X_mini_batch,
                                                  labels: Y_mini_batch})
                            print('step %d, training cost %g' % (i, train_cost))

                            test_cost = sess.run(mse, feed_dict={features: X_test,
                                                  labels: Y_test})
                            print('testing cost is %g' % (test_cost))
                    test_out = sess.run(output, feed_dict={features: X_test,
                                                  labels: Y_test})
                    np.savetxt('test_model_value.csv', test_out, delimiter=',')
                    train_out = sess.run(output, feed_dict={features: X_train[:100],
                                                  labels: Y_train[:100]})
                    np.savetxt('train_model_value.csv', train_out, delimiter=',')

targets = np.load( "target_input.npy" )
images = np.load("images.npy")

# find outliers that are unphysics
outlier1 = np.where((targets[:,0] < 0) | (targets[:,0] > 1))
outlier1 = np.asarray(outlier1)
outlier1 = np.reshape(outlier1,(-1,) )
outlier_index = np.argsort(targets,axis=0)[-100:]
outlier_index = np.concatenate((outlier_index[:,0], outlier_index[:,1]),axis=0)
outlier_index = np.concatenate((outlier_index, outlier1),axis=0)
outlier_index = np.unique(outlier_index)

# delete these outliers 
targets = np.delete(targets,outlier_index,0)
images = np.delete(images,outlier_index,0)
#targets = (targets[:,0] * 100).reshape((targets.shape[0],1))
#scaler_target = StandardScaler().fit(targets)
#targets = scaler_target.transform(targets)
#targets = targets[:,0].reshape(targets.shape[0],1)
#target_train = target_train[:,0]
#scalers = {}
#for i in range(images.shape[0]):
#    for j in range(images.shape[3]):
#        scalers[i] = StandardScaler()
#        images[i, :, :, j] = scalers[i].fit_transform(images[i, :, :, j])
#    
#images_train, images_test, targets_train, targets_test = train_test_split(
#        images, targets, test_size=0.2, random_state=21)
scaler = {}
for i in range(images.shape[0]):
        for j in range(images.shape[3]):
            scaler[i] = StandardScaler()
            images[i,:,:,j] = scaler[i].fit_transform(images[i,:,:,j])
#print(scipy.stats.skew(images))
#print(scipy.stats.kurtosis(images))
trans_target = power_transform(targets, method='yeo-johnson')

images_train, images_test, targets_train, targets_test = train_test_split(
        images, targets, test_size=0.2, random_state=21)
np.savetxt('test_real_value.csv', targets_test, delimiter=',')
np.savetxt('train_real_value.csv', targets_train, delimiter=',')
X_train, Y_train, X_test, Y_test = images_train, targets_train, images_test, targets_test
train(X_train, Y_train, X_test, Y_test)







